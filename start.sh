#!/bin/bash

set -eu

mkdir -p /app/data
chown -R cloudron:cloudron /app/data
cd /app/data

if [[ ! -f /app/data/config.json ]]; then
	    echo "==> Copying files on first run"
	        cp /app/code/config.json /app/data/config.json
fi

if [[ ! -f /app/data/messages.json ]]; then
	    echo "==> Copying files on first run"
	        cp /app/code/messages.json /app/data/messages.json
fi

cp /app/code/telegramus /app/data/
cp /app/code/runner.py /app/data/
cd /app/data
exec /usr/local/bin/gosu cloudron:cloudron python3 runner.py
