FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

ARG VERSION=4.1.2

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get install -y git binutils build-essential && \
    pip install pyinstaller

RUN curl -L https://github.com/F33RNI/GPT-Telegramus/archive/refs/tags/${VERSION}.zip -o /tmp/tmp.zip && unzip /tmp/tmp.zip -d /tmp && rm /tmp/tmp.zip
RUN mv /tmp/GPT-Telegramus-${VERSION}/* /app/code/

RUN pip install -r requirements.txt
RUN pyinstaller --specpath /app/code --distpath /app/code/dist --workpath /app/code/work     --hidden-import tiktoken_ext.openai_public     --onefile --name telegramus main.py
RUN mv dist/telegramus /tmp
RUN rm -f -R /app/code
RUN mkdir -p /app/code
RUN mv /tmp/telegramus /app/code
COPY runner.py config.json messages.json start.sh /app/code

CMD [ "/app/code/start.sh" ]
